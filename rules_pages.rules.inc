<?php
// $Id$

/**
 * @file
 * Provides rules conditions to test for various standard page views.
 */

/**
 * Implementation of hook_rules_event_info()
 */
function rules_pages_rules_event_info() {
  $items = array(
    'node_page_view' => array(
      'label' => t('Content page is going to be viewed'),
      'module' => 'Node',
      'help' => t("Note that if drupal's page cache is enabled, this event won't be generated for pages served from cache."),
      'arguments' => rules_events_node_arguments(t('viewed content'), t("content's author")),
    ),
    'term_page_view' => array(
      'label' => t('Term page is going to be viewed'),
      'module' => 'Taxonomy',
      'help' => t("Note that if drupal's page cache is enabled, this event won't be generated for pages served from cache."),
      'arguments' => rules_events_hook_taxonomy_term_arguments(t('viewed term')),
    ),
    'search_page_view' => array(
      'label' => t('Search page is going to be viewed'),
      'module' => 'System',
      'help' => t("Note that if drupal's page cache is enabled, this event won't be generated for pages served from cache."),
      'arguments' => array(
        'search_path' => array(
          'type' => 'page',
          'label' => t('Search path'),
        ),
      ) + rules_events_global_user_argument(),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_rules_data_type_info(). Should this be in flag_page?
 */
function rules_pages_rules_data_type_info() {
  return array(
    'page' => array(
      'label' => t('Page'),
      'class' => 'rules_data_type',
      'savable' => FALSE,
      'identifiable' => TRUE,
      'uses_input_form' => FALSE,
      'module' => 'System',
    ),
  );
}



